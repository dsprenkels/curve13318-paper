\section{Preliminaries}
\label{sec:prelim}

\subsection{Weierstraß, Montgomery, and twisted Edwards curves}
The typical way to introduce elliptic curves over a field $\F$ with large characteristic is
through the \emph{short Weierstraß equation}
$$
E_W: y^2 = x^3 + ax + b,
$$
where $a,b \in \F$.
As long as the discriminant $\delta = -16(4a^3 + 27b^2)$ is nonzero, this equation describes
an elliptic curve and any elliptic curve over a field $\F$ with characteristic not equal to two or three
can be described through such an equation. 
For cryptography we typically choose a field of large prime order $p$; the relevant group
in the cryptographic setup is the group of $\F_p$-rational points $E(\F_p)$.
Whenever we talk about ``the order of an elliptic curve'' in this paper we 
mean the order of this group.
The typical way to use Weierstraß curves in cryptography is to pick curve
parameter $a=-3$ for somewhat more efficient arithmetic and to represent a point
$P=(x,y)$ in Jacobian coordinates $(X:Y:Z)$ with $(x,y) = (X/Z^2, Y/Z^3)$.
Point addition is using the formulas from~\cite{BL07} (improving on~\cite{CMO98}) and uses 
$11\mathbf{M}+5\mathbf{S}+9\mathbf{a}$.
Most efficient doubling uses formulas from~\cite{Ber01} that use 
$3\mathbf{M}+5\mathbf{S}+8\mathbf{a}$.
Alternatively, one can use a ladder with differential additions, for
example using the approach from~\cite{KCK+17} that costs
$6\mathbf{M}+6\mathbf{S}+20.5\mathbf{a}$ 
per ladder step.

Using a ladder for scalar multiplication is also what Montgomery proposed in~\cite{Mon87}
for a different class of elliptic curves, so-called \emph{Montgomery curves}.
These are described through an equation of the form
$$
E_M: by^2=x^3+ax^2+x,
$$
again with $a, b \in \F$. The ``ladder step'' consisting of one differential
addition and one doubling costs  
$5\mathbf{M}+4\mathbf{S}+8\mathbf{a}$.
The formulas were shown to be complete by Bernstein in the Curve25519 paper~\cite{Ber06}. 
One peculiarity of the formulas is that
they only involve the $x$-coordinate of a point. For Diffie-Hellman protocols
this has the advantage of free point compression and decompression, but for
signatures this involves extra effort to recover the $y$-coordinate.

The most efficient complete formulas for full addition (and doubling)
are on \emph{twisted Edwards} curves~\cite{BBJ+08}, i.e., curves with equation
$$
E_{tE}: x^2+y^2=1+dx^2y^2.
$$
For the special case of $a = -1$, the formulas from~\cite{HWCD08}
need only 
$8\mathbf{M}+8\mathbf{a}$
for addition and 
$4\mathbf{M}+4\mathbf{S}+6\mathbf{a}$
for doubling. If $-1$ is a square in $\F_p$
then the formulas are complete. 
Every twisted Edwards curves is birationally equivalent to a Montgomery
curve~\cite[Thm.~3.2]{BBJ+08} and in the case of Curve25519 both shapes are
used in protocols: the Montgomery shape and corresponding ladder for
X25519 key exchange and the twisted Edwards shape for Ed25519 signatures.

\subsection{Curve13318}
The goal of this paper is to investigate the performance of complete addition and doubling
on a Weierstraß curve and compare it to the performance of Curve25519.
Many aspects contribute to the performance of elliptic-curve arithmetic and as we are
mainly interested in the impact of formulas implementing the group law, we decided to
choose a curve that is as similar to Curve25519 as possible, except that it is in Weierstraß
form and has prime order. This means that in particular, we want a curve that
\begin{itemize}
  \item is defined over the field $\F_p$ with $p = 2^{255}-19$;
  \item is twist secure (for a definition, see~\cite{Ber06} or~\cite{safecurves});
  \item has parameter $a = -3$ to support common speedups of the group law; and
  \item has small parameter $b$.
\end{itemize}

A curve with precisely these properties 
was proposed in May 2017 by Barreto on Twitter~\cite{BarretoCurve}.
Specifically, he proposed the curve with equation
$$
E : y^2 = x^3 -3x + 13318,
$$
defined over $\mathds{F}_{2^{255}-19}$. 
In a follow-up tweet Barreto clarified that the
selection criteria for this curve were
``all old SafeCurves properties (with recent improvements) plus prime order''.
Barreto did not name this curve; we will in the following
refer to it as Curve13318. 
This name at the same time points to the curve parameter $b$ and
its intended similarities to Curve25519.
The order of the group of $\F_p$-rational points on Curve13318 is
$$N = \ell = 2^{255} + 325610659388873400306201440571661405155.$$

\subsection{The Renes-Costello-Batina formulas}
\label{sec:additionformulas}

In 2016, Renes, Costello, and Batina published a set of formulas for doubling and addition on short Weierstraß curves~\cite{RCB16},
based on previous work by Bosma and Lenstra~\cite{BL95}.
The formulas are complete for all elliptic curves defined over a field with characteristic not equal to 2 or 3.
Together with the formulas published by Susella and Montrasio in 2017~\cite{SM17},
the Renes-Costello-Batina formulas are the only set of addition formulas
for prime-order Weierstraß curves that is proven to be complete.

Because we implement variable-basepoint scalar multiplication on a curve with $a=-3$,
we will use the algorithms for addition and doubling from~\cite[Section 3.2]{RCB16}.
The relevant complete formulas for (projective) point addition are
\begin{align*}
    X_3 &= (X_1Y_2 + X_2Y_1)\left(Y_1Y_2 + 3(X_1Z_2 + X_2Z_1 - bZ_1Z_2)\right) \\
        &\quad -3(Y_1Z_2 + Y_2Z_1)\left(b(X_1Z_2 + X_2Z_1) - X_1X_2 - 3Z_1Z_2\right)\text{,} \\
    Y_3 &= 3(3X_1X_2 - 3Z_1Z_2)\left(b(X_1Z_2 + X_2Z_1) - X_1X_2 - 3Z_1Z_2\right)\\
        &\quad 
        \hspace{-4ex}
        +\left(Y_1Y_2 - 3(X_1Z_2 + X_2Z_1 - bZ_1Z_2)\right)\left(Y_1Y_2 + 3(X_1Z_2 + X_2Z_1 - bZ_1Z_2)\right)\text{,}\\
    Z_3 &= (Y_1Z_2 + Y_2Z_1)\left(Y_1Y_2 - 3(X_1Z_2 + X_2Z_1 - bZ_1Z_2)\right)\\
        &\quad +(X_1Y_2 + X_2Y_1)(3X_1X_2 - 3Z_1Z_2)\text{.}
\end{align*}

In~\cite{RCB16}, the formula for addition is implemented through 43 distinct operations, 
specifically $12\mathbf{M} + 2\mathbf{m_b} + 29\mathbf{a}$.
The algorithm used to compute the addition (\Add{}) is listed in \Cref{alg:add}.

\begin{algorithm}[h]
\caption{Renes-Costello-Batina formula for $a=-3$. Used for exception-free addition on Curve13318.}\label{alg:add}
\begin{algorithmic}
\Procedure{Add}{$(X_1 : Y_1 : Z_1)$, $(X_2 : Y_2 : Z_2)$}
% \Comment{Compute $(X_1 : Y_1 : Z_1)+(X_2 : Y_2 : Z_2)$}
\vspace{-0.5em}
\begin{multicols}{3}
\par $v_{ 1} \gets X_1 \cdot X_2$
\par $v_{ 2} \gets Y_1 \cdot Y_2$
\par $v_{ 3} \gets Z_1 \cdot Z_2$
\par $v_{ 4} \gets X_1 + Y_1$
\par $v_{ 5} \gets X_2 + Y_2$
\par $v_{ 6} \gets v_{ 4} \cdot v_{ 5}$
\par $v_{ 8} \gets v_{ 6} - v_{ 7}$
\par $v_{ 9} \gets Y_1 + Z_1$
\par $v_{10} \gets Y_2 + Z_2$
\par $v_{11} \gets v_{ 9} \cdot v_{10}$
\par $v_{13} \gets v_{11} - v_{12}$
\par $v_{14} \gets X_1 + Z_1$
\par $v_{15} \gets X_2 + Z_2$
\par $v_{16} \gets v_{14} \cdot v_{15}$
\par $v_{17} \gets v_{ 1} + v_{ 3}$
\par $v_{18} \gets v_{16} - v_{17}$
\par $v_{19} \gets b \cdot v_{ 3}$
\par $v_{20} \gets v_{19} - v_{18}$
\par $v_{21} \gets v_{20} + v_{20}$
\par $v_{22} \gets v_{20} + v_{21}$
\par $v_{23} \gets v_{ 2} - v_{22}$
\par $v_{24} \gets v_{ 2} + v_{22}$
\par $v_{25} \gets b \cdot v_{18}$
\par $v_{26} \gets v_{ 3} + v_{ 3}$
\par $v_{27} \gets v_{26} + v_{ 3}$
\par $v_{28} \gets v_{25} - v_{27}$
\par $v_{29} \gets v_{28} - v_{ 1}$
\par $v_{30} \gets v_{29} + v_{29}$
\par $v_{31} \gets v_{30} + v_{29}$
\par $v_{33} \gets v_{ 1} + v_{ 1}$
\par $v_{33} \gets v_{32} + v_{ 1}$
\par $v_{34} \gets v_{33} - v_{27}$
\par $v_{35} \gets v_{13} \cdot v_{31}$
\par $v_{36} \gets v_{31} \cdot v_{34}$
\par $v_{37} \gets v_{23} \cdot v_{24}$
\par $v_{38} \gets v_{36} + v_{37}$
\par $v_{39} \gets v_{ 8} \cdot v_{24}$
\par $v_{40} \gets v_{39} - v_{35}$
\par $v_{41} \gets v_{13} \cdot v_{23}$
\par $v_{42} \gets v_{ 8} \cdot v_{33}$
\par $v_{43} \gets v_{41} + v_{42}$
\end{multicols}
\vspace{-0.5em}
\par $X_3 \gets v_{40}$
\par $Y_3 \gets v_{38}$
\par $Z_3 \gets v_{43}$
\vspace{+0.5em}
\par \Return $(X_3 : Y_3 : Z_3)$
\EndProcedure
\end{algorithmic}
\end{algorithm}

Correspondingly, the complete formulas for doubling are
\begin{align*}
    X_3 &= 2XY(Y^2 + 3(2XZ - bZ^2)) - 6XZ(2bXZ - X^2 - 3Z^2)\text{,} \\
    Y_3 &= (Y^2 - 3(2XZ - bZ^2))(Y^2 + 3(2XZ - bZ^2)) \\
        &\quad  + 3(3X^2 - 3Z^2)(2bXZ - X^2 - 3Z^2)\text{,} \\
    Z_3 &= 8Y^3Z\text{.}
\end{align*}
The cost of the doubling formulas is $8\mathbf{M} + 3\mathbf{S} + 2\mathbf{m_b} + 21\mathbf{a}$. 
The algorithm for doubling (\Double{}) is listed in~\Cref{alg:double}.

\begin{algorithm}[h]
\caption{Renes-Costello-Batina formula for $a=-3$. Used for exception-free doubling on Curve13318.}\label{alg:double}
\begin{algorithmic}
\Procedure{Double}{$(X : Y : Z)$}
% \Comment{Compute $[2](X : Y : Z)$}
\vspace{-0.5em}
\begin{multicols}{3}
\par $v_{ 1} \gets X \cdot X$
\par $v_{ 2} \gets Y \cdot Y$
\par $v_{ 3} \gets Z \cdot Z$
\par $v_{ 4} \gets X \cdot Y$
\label{other:mul1}
\par $v_{ 5} \gets v_{ 4} + v_{ 4}$
\par $v_{ 6} \gets X \cdot Z$
\par $v_{ 7} \gets v_{ 6} + v_{ 6}$
\par $v_{ 8} \gets b \cdot v_{ 3}$
\par $v_{ 9} \gets v_{ 8} - v_{ 7}$
\par $v_{10} \gets v_{ 9} + v_{ 9}$
\par $v_{11} \gets v_{10} - v_{ 9}$
\par $v_{12} \gets v_{ 2} - v_{11}$
\par $v_{13} \gets v_{ 2} + v_{11}$
\par $v_{14} \gets v_{12} \cdot v_{13}$
\par $v_{15} \gets v_{ 5} \cdot v_{12}$
\par $v_{16} \gets v_{ 3} + v_{ 3}$
\par $v_{17} \gets v_{ 3} + v_{16}$
\par $v_{18} \gets b \cdot v_{ 7}$
\par $v_{19} \gets v_{18} - v_{17}$        
\par $v_{20} \gets v_{ 1} - v_{19}$
\par $v_{21} \gets v_{20} + v_{20}$
\par $v_{22} \gets v_{20} + v_{21}$
\par $v_{23} \gets v_{ 1} + v_{ 1}$
\par $v_{24} \gets v_{23} + v_{ 1}$
\par $v_{25} \gets v_{24} - v_{17}$
\par $v_{26} \gets v_{22} \cdot v_{25}$
\par $v_{27} \gets v_{14} + v_{26}$
\par $v_{28} \gets Y \cdot Z$
\par $v_{29} \gets v_{28} + v_{28}$
\par $v_{30} \gets v_{22} \cdot v_{29}$
\par $v_{31} \gets v_{15} - v_{30}$
\par $v_{32} \gets v_{ 2} \cdot v_{34}$
\par $v_{33} \gets v_{32} + v_{32}$
\par $v_{34} \gets v_{33} + v_{33}$
\end{multicols}
\vspace{-0.5em}
\par $X_3 \gets v_{31}$
\par $Y_3 \gets v_{27}$
\par $Z_3 \gets v_{34}$
\vspace{+0.5em}
\par \Return $(X_3 : Y_3 : Z_3)$
\EndProcedure
\end{algorithmic}
\end{algorithm}

We can reduce the cost of the doubling algorithm by erasing (some of) the multiplications $v_{1}$, $v_{4}$, $v_{6}$, $v_{28}$, 
using the rule that $2\alpha\beta = (\alpha + \beta)^2 - \alpha^2 - \beta^2$.
By applying this rule, we trade $1\mathbf{M} + 1\mathbf{a}$ for $1\mathbf{S} + 3\mathbf{a}$. 
As we will describe in Section~\ref{sec:implementation},
this trick is beneficial only on the \emph{Haswell} platform, 
