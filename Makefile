curve13318.pdf: algorithms.tex curve13318.tex conclusion.tex implementation.tex intro.tex prelim.tex results.tex refs.bib alg_sb_add.tex alg_sb_double.tex alg_h_add.tex alg_h_double.tex
	pdflatex -shell-escape curve13318.tex
	bibtex curve13318
	pdflatex -shell-escape curve13318.tex
	pdflatex -shell-escape curve13318.tex

#alg_sb_add.tex alg_sb_double.tex alg_h_add.tex alg_h_double.tex: gen_algorithms.py
#	./gen_algorithms.py

.PHONY: clean

clean:
	-rm *.aux
	-rm *.log
	-rm *.fls
	-rm curve13318.out
	-rm curve13318.pdf
	-rm curve13318.bbl
	-rm curve13318.blg
	-rm -r _minted-curve13318
