\section{Implementation}
\label{sec:implementation}

In order to get a comprehensive benchmark for the performance of complete arithmetic on Curve13318, 
we optimized variable-basepoint scalar multiplication 
on the Intel \emph{Sandy Bridge} and \emph{Haswell} microarchitectures, 
as well as the ARM \emph{Cortex M4} processor.

The high-level structure of the scalar multiplication is shared among all three implementations. 
First---before operating on the key $k$---we validate the input point $P$,
by checking whether $P$ satisfies $y_P^2 = x_P^3 - 3x_P + 13318$. 
Because we have not defined any encoding for the neutral element $\mathcal{O}$, 
this check will implicitly validate that $P \ne \mathcal{O}$.

For the scalar-multiplication core, 
we use a left-to-right signed-window double-and-add algorithm, with $w=5$. 
This algorithm is listed in \Cref{alg:doubleandadd}.
The subroutine $\textsc{RecodeSignedWindow}_5$
computes a vector of coefficients $k' = (k'_0,\dots,k'_{50})$, 
such that $k = k'_0 + 32k'_1 + \dots, + 2^{250}k'_{50}$ and
$k'_i \in \{-16,\dots,15\}$.

The table lookup is implemented in a traditional scanning fashion:
selecting the required value using a bitwise AND operation.
Where we use an unsigned representation,
we compute the conditional negation of $Y$
by negating $Y$ and selecting the correct result using bitwise operations. When using floating points,
we use a single XOR operation to conditionally flip the sign bit.
These operations are---as well as the rest of the code---implemented in constant-time.

\begin{algorithm}
\caption{Signed double-and-add describe the used functions}\label{alg:doubleandadd}
\begin{algorithmic}[1]
\Function{DoubleAndAdd}{$k,P$}%\Comment{Compute $[k]P$}
\State $\mathbf{T} \gets (\mathcal{O}, P, \ldots, [16]P)$\Comment{Precompute $([2]P, \ldots, [16]P)$}
  \State $k'\gets \textsc{RecodeSignedWindow}_5(k)$
\State $R\gets \mathcal{O}$
\For{$i$ \textbf{from} $50$ \textbf{down to} $0$}
    \State $R\gets [32]R$\Comment{$5$ point doublings}
    \State $Q \gets T_{|k'_i|}$ \Comment{Constant-time lookup from $\mathbf{T}$}
    \State $Q \gets (-1)^{k'_i}$Q \Comment{Constant-time conditional negation}
    \State $R\gets R + Q$\Comment{Point addition}
    \EndFor
\State \textbf{return} $R$\Comment{$R = (X_R : Y_R : Z_R)$}
\EndFunction
\end{algorithmic}
\end{algorithm}

At the end of the double-and-add algorithm, 
we end up with a representation of $R = [k]P$ in projective coordinates. 
We compute the affine representation of $x_R$ and $y_R$ by computing the inverse of $Z_R$. 
Like most implementations of Curve25519 scalar multiplication, 
we use Fermat's little theorem and raise $Z_R$ to the power $2^{255} - 21$ to obtain $Z_R^{-1}$. 
We chose not to exploit the optimization described in~\cite{BY19}, 
because previous implementations have not had the opportunity to implement this technique; 
exploiting this invention would give us an unfair advantage.

In the following subsections we describe the architecture-specific optimizations
of field arithmetic required to implement the Renes-Costello-Batina formulas and
in particular our vectorization strategy on Intel processors.

\subsection{Sandy Bridge}

The first implementation we present is based on the \emph{Sandy Bridge} microarchitecture. 
Sandy Bridge is Intel's first microarchitecture featuring \emph{Advanced Vector Extensions} (AVX). 
In addition to $2\times$-parallel 64-bit integer arithmetic, 
AVX supports $4\times$-parallel double-precision floating-point arithmetic. 
Because the multiplications and squarings in the Renes-Costello-Batina formulas 
can be conveniently grouped in batches of 4, 
we will be using the $4\times$-parallel floating-point arithmetic 
on $256$-bit \texttt{ymm} vector registers.

\subheading{Representation of prime-field elements.}
Using doubles with 53-bit mantissa, we can emulate integer registers of 53 bits. 
To guarantee that no rounding errors occur in the underlying floating-point arithmetic, 
we use carry chains\footnote{Also called ``coefficient reduction''.} 
to reduce the amount of bits in each register before performing operations that might overflow.
Building on this approach, \cite{Ber06} recommends---%
but does not implement---%
\emph{radix-$2^{21.25}$} redundant representation, 
based on the arithmetic described in~\cite{Ber04}.

We use precisely this representation and represent a field element $f$ through $12$ 
signed double-precision floating-point values $f_0,\dots,f_{11}$. 
For every $f_i$, its base $b_i$ is defined by $b_i = 2^{\lceil21.25i\rceil}$.
Doubles already store their base in the exponent, which is large enough for our purposes. 
Therefore, we do not have to consider the base when evaluating $f$'s value. 
Indeed, the value is computed by just computing the sum of the limbs:
\begin{equation*}
    f = \sum_{i=0}^{11} f_i
\end{equation*}

\subheading{Coefficient reduction.}
The Intel architecture supports no native modulo operation on floating points. 
Instead we extract a limb $f_i$'s top bits by subsequently adding and subtracting a large constant $c_i = 3 \cdot 2^{51}b_{i+1}$, 
forcing the processor to discard the lower mantissa bits.

In code, each carry step needs 5 instructions to perform this routine. 
In Listing~\ref{lst:fe12_carrystep}, the $4\times$-vectorized carry step from $f_0$ to $f_1$ is shown. 
To reduce $f_{12}$ back to $f_0$, we multiply by $19 \cdot 2^{-255}$, 
which is implemented using a regular \mintinline{nasm}{vmulpd ymmX, [rel .reduceconstant]} instruction.

\begin{listing}[h]
\caption{Single carry step for radix $2^{21.25}$ from limb $f_0$ to limb $f_1$.}
\label{lst:fe12_carrystep}
\begin{minted}[fontsize=\small, linenos, mathescape]{nasm}
; Inputs:
;   - ymm0: $f_0$
;   - [rel .precisionloss0]: times 4 dq 0x3p73 ($c_0 = 3\cdot2^{51}\cdot2^{22}$)
; Outputs:
;   - ymm0: $f_0$
;   - ymm1: $f_1$
vmovapd ymm14, yword [rel .precisionloss0]  ; load $c_0$
vaddpd ymm15, ymm0, ymm14                   ; $z' \gets \mathrm{round}(f_0 + c_0)$
vsubpd ymm15, ymm15, ymm14                  ; $t \gets \mathrm{round}(z' - c_0)$
vaddpd ymm1, ymm1, ymm15                    ; $f_1 \gets \mathrm{round}(f_1 + t)$
vsubpd ymm0, ymm0, ymm15                    ; $f_0 \gets \mathrm{round}(f_0 - t)$
\end{minted}
\end{listing}

% \begin{listing}[h]
% \caption{Single carry step for radix $2^{21.25}$ from limb $0$ to limb $1$.}
% \label{lst:fe12_carrystep}
% \begin{minted}[fontsize=\small, linenos, mathescape]{nasm}
% ; Arguments:
% ;   %1: carry to this limb $f_{i+1}$
% ;   %2: carry from this limb $f_i$
% ;   %3: label with precision-loss constant $c_i$
% vmovapd ymm14, yword [rel %3]                   ; load $c_0$
% vaddpd ymm15, %2, ymm14                 ; $z' \gets \mathrm{round}(f_0 + c_0)$
% vsubpd ymm15, ymm15, ymm14                  ; $t \gets \mathrm{round}(z' - c_0)$
% vaddpd %1, %1, ymm15                    ; $f_1 \gets \mathrm{round}(f_1 + t)$
% vsubpd %2, %2, ymm15                    ; $f_0 \gets \mathrm{round}(f_0 - t)$
% \end{minted}
% \end{listing}

All micro-operations (µops) corresponding to the arithmetic instructions in Listing~\ref{lst:fe12_carrystep} execute on port 1 of Sandy Bridge's back end. Furthermore, every \texttt{v\{add,sub\}pd} instruction has a latency of 3 cycles (cc). Consequently, the latency of one carry step is the sum of the latencies of instructions 2\,--\,4, i.e. the latency is $3+3+3 = 9\unit{cc}$. Still, the reciprocal throughput is only $4\unit{cc}$.

In a sequential carry chain the back end is stalled most of the time due to data hazards. We expect a single carry chain to use $9 \cdot 14 = 126\unit{cc}$ or $31.5\unit{cc}$ per lane. Even in a twice interleaved carry chain, the latency is still $63\unit{cc}$,  while the reciprocal throughput is still only $56\unit{cc}$. In other words, the twice interleaved case still suffers from data hazards. 

To overcome this, we implement a triple interleaved carry chain, as displayed in Figure~\ref{fig:fe12_carrychaininterleaved}. In this case, the latency is reduced to $45\unit{cc}$, while the reciprocal throughput is $60\unit{cc}$. Conversely the bottleneck is not the latency, but the reciprocal throughput of the carry chain, of which the lower bound is $15\unit{cc}$ per lane.
\begin{figure}[H]
\centering
\begin{alignat*}{12}
f_0 &\rightarrow &f_1 &\rightarrow &f_2    &\rightarrow &f_3    &\rightarrow &f_4 &\rightarrow &f_5&,\\
f_4 &\rightarrow &f_5 &\rightarrow &f_6    &\rightarrow &f_7    &\rightarrow &f_8 &\rightarrow &f_9&,\\
f_8 &\rightarrow &f_9 &\rightarrow &f_{10} &\rightarrow &f_{11} &\rightarrow &f_0 &\rightarrow &f_1&
\end{alignat*}
\caption{Triple interleaved 12-limb carry chain.}
\label{fig:fe12_carrychaininterleaved}
\end{figure}



\subheading{Multiplication.}
For radix-$2^{21.25}$, we use basic $4\times$ parallel Karatsuba multiplication~\cite{KO62}, using inspiration from~\cite{HS15}. An inconvenience introduced by implementing Karatsuba using floating points, is that the shift-by-128-bit operations cannot be optimized out. Instead, we have to explicitly multiply some limbs by~$2^{\pm128}$. This costs $23$ extra multiplication ops (implemented using $12$ \texttt{vmulpd}s, and $11$ \texttt{vandpd}s).
Still, the Karatsuba implementation, which contains $131$ \texttt{vmulpd} instructions, was measured to be $8\%$ faster than the schoolbook method (which contains $155$ \texttt{vmulpd} instructions).


\subheading{Vectorization strategy.}
We group the multiplications from both algorithms in three batches each, which
have been chosen such that the complexity of the operations
in-between the multiplications minimized.
The resulting algorithms are given in Algorithms~\ref{alg:add_asm_sandybridge} and~\ref{alg:double_asm_sandybridge}.

\begin{algorithm}[tb!]
\caption{
    Algorithm for point addition for Curve13318 as implemented on the
    Sandy Bridge microarchitecture.
    \sbcaptionextra
}
\label{alg:add_asm_sandybridge}
\begin{algorithmic}
\Procedure{Add}{$X_1$, $Y_1$, $Z_1$, $X_2$, $Y_2$, $Z_2$}
\State
\begin{tabular}{p{0.225\textwidth}p{0.225\textwidth}p{0.225\textwidth}p{0.225\textwidth}}
  \input{alg_sb_add}
\end{tabular}
\vskip 1em
\State $X_3 \gets v_{40}$
\State $Y_3 \gets v_{38}$
\State $Z_3 \gets v_{43}$
\EndProcedure
\end{algorithmic}
\end{algorithm}


\begin{algorithm}[tb!]
\caption{
    Algorithm for point doubling for Curve13318 as implemented on the
    Sandy Bridge microarchitecture.
    \sbcaptionextra
}
\label{alg:double_asm_sandybridge}
\begin{algorithmic}
\Procedure{Double}{$X$, $Y$, $Z$}
\State
\begin{tabular}{p{0.225\textwidth}p{0.225\textwidth}p{0.225\textwidth}p{0.225\textwidth}}
  \input{alg_sb_double}
\end{tabular}
\vskip 1em
\State $X_3 \gets v_{31}$
\State $Y_3 \gets v_{27}$
\State $Z_3 \gets v_{34}$
\EndProcedure
\end{algorithmic}
\end{algorithm}

In particular, we cannot optimize the squaring operations in \Double{} using the $2\alpha\beta = (\alpha + \beta)^2 - \alpha^2 - \beta^2$ rule, because $\alpha + \beta$ has too little headroom to be squared without doing an additional carry chain.

Because we cannot perform shift operations on floating-point values, and because the reciprocal throughput of \texttt{vmulpd} and \texttt{v\{add,sub\}pd} are both $1\unit{cc}$, we replace all chained additions by multiplications.
% This eliminates $\{v_{21}, v_{26}, v_{30}, v_{32}\}$ from \Add{}, and $\{v_{7}, v_{10}, v_{16}, v_{21}, v_{23}\}$ from \Double{}.
This substitutes $8\mathbf{a}$ for $4\mathbf{m}$ in \Add{}, and $10\mathbf{a}$ for $5\mathbf{m}$ in \Double{}.

Last, we found that shuffling the \texttt{ymm} registers turns out to be
relatively weak and expensive.
That is because Sandy Bridge has no arbitrary shuffle instruction
(like the \texttt{vpermq} instruction in AVX2).
To shuffle every value in a \texttt{ymm} register into the correct lane,
we would need at least two µops on port 5.
Then it is cheaper to put all the values in the first lane, and
accept that most of the additions and subtractions are not batched.



\subsection{Haswell}
\label{sec:haswell}

The more recent \emph{Haswell} microarchitecture from Intel supports \emph{Advanced Vector Extensions 2} (AVX2). Haswell's AVX2 is more powerful than its predecessor. First, because AVX2 allows for $4\times$ parallel 64-bit integer arithmetic; and second, because addition and subtraction operations---using the \texttt{vp\{add,sub\}q} instructions---have a reciprocal throughput of only $0.5\unit{cc}$. Together with the other instructions in AVX2, Haswell lends itself for efficient $4\times$ parallel 64-bit integer arithmetic.

% \begin{Verbatim}
%     - H has AVX2: that is four-way 64-bit integer registers
%         - vpmuludq: 32x32-bit multiplication into 64-bit result (~throughput: 1)
%         - vp{add,sub}q: (throughput: 0.5)
%     - Double interleaved carry chain from [Sandy2x]
% \end{Verbatim}

\subheading{Representation of prime-field elements.}
We use the \emph{radix-$2^{25.5}$} redundant representation, which was introduced in~\cite{BS12}. The representation stores an integer $f$ into $10$ \emph{unsigned}\footnote{
When we use \emph{signed} limbs, we need---for the coefficient reduction---an instruction that shifts packed quadwords to the right while shifting in sign bits. Such an arithmetic shift operation---which would be called \texttt{vpsraq}---has never been implemented for the Haswell microarchitecture. Indeed, the first occurrence of the \texttt{vpsraq}-instruction was in AVX-512, in the Knight's Landing and Skylake-X microarchitectures.
} 64-bit limbs, with each base $b_i = 2^{\lceil25.5i\rceil}$. Then the value of $f$ is given by
\begin{equation*}
    f = \sum_{i=0}^{9} b_if_i%
    % = \sum_{i=0}^{9} 2^{\lceil25.5i\rceil}f_i
    \text{.}
\end{equation*}

% \begin{Verbatim}
%     - Using unsigned radix 2^25.5 w/ 10 limbs based on [NEONcrypto,Sandy2x]
%         - Unsigned because of the lack of a vpsradq instruction
% \end{Verbatim}

\subheading{Coefficient reduction.}
For coefficient reduction in radix $2^{25.5}$, we use the carry chain described
by Sandy2x~\cite{Cho16}, adapted to AVX2. It is shown in
Listing~\ref{lst:fe10_carrystep}.
\begin{listing}[h]
\caption{Single carry step for radix $2^{25.5}$.}
\label{lst:fe10_carrystep}
\begin{minted}[fontsize=\small, linenos, mathescape]{nasm}
; Inputs:
;   - ymm0: $f_0$
;   - [rel .MASK26]: times 4 dq 0x3FFFFFF
; Outputs:
;   - ymm0: $f_0$
;   - ymm1: $f_1$
vpsrlq ymm15, ymm0, 26                   ; $t \gets \lfloor2^{-26}f_0\rfloor$
vpaddq ymm1, ymm1, ymm15                 ; $f_1 \gets f_1 + t$
vpand ymm0, ymm0, yword [rel .MASK26]    ; $f_0 \gets f_0 \mod 2^{26}$
\end{minted}
\end{listing}

One carry step uses only 3 µops, each of which can execute on a separate port.
Consequently, the reciprocal throughput of a single carry step is $1\unit{cc}$, while the latency of a carry step is $2\unit{cc}$.
Therefore, it is optimal to implement the coefficient reduction using a twice interleaved carry chain, as visualized in~\Cref{fig:fe10carrychaininterleaved}.
\begin{figure}[H]
\centering
\begin{align*}
&f_0 \rightarrow f_1 \rightarrow f_2 \rightarrow f_3 \rightarrow f_4 \rightarrow f_5 \rightarrow f_6,\\
&f_5 \rightarrow f_6 \rightarrow f_7 \rightarrow f_8 \rightarrow f_9 \rightarrow f_0 \rightarrow f_1
\end{align*}
\caption{Twice interleaved 10-limb carry chain.}
\label{fig:fe10carrychaininterleaved}
\end{figure}

\subheading{Multiplication and squaring.}
For the multiplication of numbers in radix $2^{25.5}$, we adapt the multiplication routine from Sandy2x for AVX2. The multiplication routine uses the schoolbook method of multiplication. It contains $109$ \texttt{vpmuludq} instructions. Of these $109$
instructions, $9$ \texttt{vpmuludq}s are used to precompute $19\cdot\{g_1, g_2, \ldots, g_9\} \equiv 2^{255}\cdot\{g_1, g_2, \ldots, g_9\}$, where $g$ is the second input operand. These values will be used for the limbs in the result that wrap around the
field modulus, as is described in the Sandy2x paper~(\cite{Cho16}).

In addition to the multiplication routine, we implement an optimized routine for squaring operations, based on the multiplication routine, that we will use in the next section.

Furthermore, we looked at the possibility of using Karatsuba multiplication, instead of using the schoolbook method. In this endeavor, we chose the Karatsuba \emph{base} $B = 2^{153}$, i.e.\ we split the inputs into one part of 6 limbs, and one part of 4 limbs. We execute the Karatsuba algorithm to obtain a 19-limb value $h_u$, which is the \emph{uncarried} result. To carry the high limbs from $h_u$ onto the lower limbs, we multiply the high limbs by $19$ using $3$ \texttt{vpaddq}s and $1$ \texttt{vpsllq} per limb; then we accumulate the results onto the lower limbs, yielding our carried product $h$.

In the Karatsuba routine, the port pressure is better divided, with $97$ µops on port 0 and $149$ µops on ports 1 and 5, relative to the schoolbook method, with $109$ µops on port 0 and $90$ on ports 1 and 5. However, the Karatsuba multiplication routine performs considerably worse than the Schoolbook method. 
Presumably, the CPU's front end cannot keep up with the added bulk of instructions.

\subheading{Why not \texttt{mulx}?}
% I looked the author-notation up in their paper. They use "et al." for short.
In~\cite{OLH+18}, Oliveira et~al.\ make use of the Bit Manipulation Instruction Set 2 (BMI2) extension in Haswell. 
BMI2 introduces the instruction \texttt{mulx}, which allows for unsigned multiply with arbitrary destination registers 
(instead of always storing the result in \texttt{rdx:rax}). 
Using this with a packed radix-$2^{64}$ representation, field multiplication and squaring can be sped up quite a lot.
However, experiments showed that the penalty introduced by more expensive additions/subtractions beat the performance gain achieved by using \texttt{mulx}.

\subheading{Vectorization strategy.}
Similar as in the implementation for Sandy Bridge, we batched all multiplications in the \Add{} algorithm into three different batches.
The complete vectorization strategy is given in 
Algorithms~\ref{alg:add_asm_haswell} and~\ref{alg:double_asm_haswell}.

\enlargethispage{-5\baselineskip}
\begin{algorithm}[tb!]
\caption{
    Algorithm for point addition for Curve13318 as implemented on the
    Haswell microarchitecture.
    \hcaptionextra
}
\label{alg:add_asm_haswell}
\begin{algorithmic}
\Procedure{Add}{$X_1$, $Y_1$, $Z_1$, $X_2$, $Y_2$, $Z_2$}
\State
\begin{tabular}{p{0.225\textwidth}p{0.225\textwidth}p{0.225\textwidth}p{0.225\textwidth}}
  \input{alg_h_add}
\end{tabular}
\vskip 1em
\State $X_3 \gets v_{40}$
\State $Y_3 \gets v_{38}$
\State $Z_3 \gets v_{43}$
\EndProcedure
\end{algorithmic}
\end{algorithm}


\begin{algorithm}[tb!]
\caption{
    Algorithm for point doubling for Curve13318 as implemented on the
    Haswell microarchitecture.
    \hcaptionextra
}
\label{alg:double_asm_haswell}
\begin{algorithmic}
\Procedure{Double}{$X$, $Y$, $Z$}
\State
\begin{tabular}{p{0.30\textwidth}p{0.20\textwidth}p{0.20\textwidth}p{0.20\textwidth}}
  \input{alg_h_double}
\end{tabular}
\vskip 1em
\State $X_3 \gets v_{31}$
\State $Y_3 \gets v_{27}$
\State $Z_3 \gets v_{34}$
\EndProcedure
\end{algorithmic}
\end{algorithm}



In the implementation of \Double{}, we applied the squaring trick described in \Cref{sec:additionformulas}, and rewrite $v_7 = 2XZ = (X+Z)^2 - X^2 - Z^2$. After replacing the multiplication $v_6$ by a squaring, we can replace the first
of the three multiplication batches in \Double{} with
a batched squaring operation, that computes the values $\{(X+Z)^2, v_1, v_2, v_3\}$.

We realize that in both algorithms we can reduce the amount of shuffle operations needed, by unpacking the values from their SIMD registers after the first batched operations, and using the general-purpose instructions for many
cheap operations, i.e.\ additions, subtractions, triplings\footnote{%
Using \mintinline{nasm}{lea r64, [2*r64 + r64]} instructions.
},
and multiplies with $b$.
This way, we eagerly compute the core of the algorithm, leaving only two batched multiplications and a few additions.
After repacking the values into the \texttt{ymm}-bank, 
we execute the remainder of the algorithm, 
including the two other multiplication batches.

\subsection{ARM Cortex M4}

\subheading{Field arithmetic.}
For the ARM Cortex M4, we reused the finite field arithmetic from Haase and Labrique~\cite{HL19}. For field elements, they use a packed representation in radix $2^{32}$. We refer to their paper for the details of the field arithmetic, which can be summarized as cleverly exploiting the magnificent powers of the \texttt{umlal} and \texttt{umaal} instructions.

One function we added was \texttt{fe25519_mul_u32_asm}, used for multiplication with small constants. It was based on Fujii's code~\cite[Listing~3.2]{Fuj18:masterthesis}, which was in turn based on~\cite{SanSig16}.

\subheading{Application of formulas.}
On top of the field arithmetic, we implemented the \Add{} and \Double{ algorithms using function calls to the underlying field operations.
Because---compared to multiplications---field additions are relatively expensive, there is no benefit in using the $2\alpha\beta = (\alpha + \beta)^2 - \alpha^2 - \beta^2$ trick.
However, multiply-with-small-constant operations are relatively cheap, so we replaced any chained additions (like the $v_b \gets v_a + v_a; \text{ then } v_a \gets v_b + v_a$ pattern) by multiplications (i.e.\ $v_a \gets 3v_a$).
No other modifications were introduced. Even the order of the operations has been kept to the original.

% \begin{Verbatim}
%     - Note: not protected against power-analysis
% \end{Verbatim}
%
% \begin{Verbatim}
%     - Used arithmetic from \cite{HaaLab19} with function calls.
%     - Used the mul_32 function from [DHHHPSS15]
%     - Did nothing of interest.
%     - Just implemented the formulas.
%     - How to word this properly?
% \end{Verbatim}
