\section{Performance results}
\label{sec:results}

The complete scalar multiplication algorithm was tested and benchmarked on 
Intel Core i7-2600 (Sandy Bridge),
Intel Core i5-3210 (Ivy Bridge),
Intel Core i7-4770 (Haswell),
and the ARM STM32F407 (Cortex-M4).
On the Intel processors, all measurements were done with Turbo Boost disabled, 
all Hyper-Threading cores shut down, and with the CPU clocked at the maximum
nominal frequency.
The STM32F407 device was run with its default settings,
as listed in the datasheet~\cite{STM32F407}
(i.e.~clocked from the 16\unit{MHz} internal RC-oscillator).
We list the benchmarking results in Table~\ref{tab:benchmarks}. As expected,
none of our implementations exceed the performance of Curve25519.

\def\arraystretch{1.1}
\setlength\tabcolsep{6pt}
\ctable[
    caption = {Measured cycle counts
    of the variable-basepoint scalar-multiplication routines
    on the {Sandy Bridge} (SB), {Ivy Bridge} (IB), {Haswell} (H) and {Cortex M4} (M4) architectures.
    },
    label = tab:benchmarks
]{lrrrr}{
    \tnote[a]{As reported in the respective publication.}
    \tnote[b]{From own measurements.}
    \tnote[c]{As reported in~\cite{FL15}.
    This publication expressed their benchmarks in \unit{kcc}.
    As such, this value has been padded with zeros.}
    \tnote[d]{Cycle counts reported on Bernstein and Lange's eBACS website~\cite{EBACS};
    included for the sake of completeness.
    The SB, IB and H measurements were selected from the
    tables for the
    \texttt{h6sandy}, \texttt{manny613} and \texttt{genji202} machines respectively.
    At the moment of writing,
    it is unclear to the authors which implementations were used to construct
    these cycle counts.
    }
}{
    \hline
    \textbf{Implementation} & \textbf{SB} & \textbf{IB} & \textbf{H} & \textbf{M4} \\
    \hline
    Chou16~\cite{Cho16} & $159\,128$\tmark[a] & $156\,995$\tmark[a] & $155\,823$\tmark[b] & -- \\
    Faz-Hernández-Lopez15~\cite{FL15} & -- & -- & $\approx156\,500$\tmark[c] & -- \\
    OLHF18~\cite{OLH+18} & -- & -- & $138\,963$\tmark[a] & -- \\
    %                                              ^ 138,963
    % Their code is at https://github.com/armfazh/rfc7748_precomputed
    % So in <https://hyperelliptic.org/tanja/lc17/ascrypto/day1/slides/lopez-software.pdf> Lopez reports 127kcc, using MULX+ADCX/ADOX, but Haswell does not support ADX (only BMI2).
    Fujii-Aranha19~\cite{FA17} & -- & -- & -- & $907\,240$\tmark[a]\\
    Haase-Labrique19~\cite{HL19} & -- & -- & -- & $625\,358$\tmark[a]\\
    Curve13318~(\textbf{this work}) & $389\,546$\tmark[b] & $382\,966$\tmark[b] & $204\,643$\tmark[b] & $1\,797\,451$\tmark[b] \\
    Ed25519 verify & $221\,988$\tmark[d] & $206\,080$\tmark[d] & $184\,052$\tmark[d] & -- \\
    % SB: SUPERCOP h6sandy
    % IB: SUPERCOP manny613
    % H: SUPERCOP genji202
    \hline \hline
    \textbf{slowdown} & $2.45\times$ & $2.44\times$ & $1.47\times$ & $2.87\times$
    % \textbf{slowdown} & $2.5\times$ & $2.4\times$ & $1.5\times$ & $2.9\times$
}

It can immediately be seen that the slowdown factor is dependent on the platform.
In particular, the Haswell implementation of scalar multiplication on Curve13318 
performs, also relatively speaking, much better than the others.
The source of this is seems to be that Algorithms~\ref{alg:add} and~\ref{alg:double}
lend themselves for very efficient 4-way parallelization,
which is not supported by Curve25519's ladder algorithm.
Through AVX2, 4-way parallelization is very powerful on Haswell,
whereas on the other platforms it is not, at least not to the same extent.
This makes it possible to write
a Haswell implementation that is significantly faster than the others.

\subheading{The cost of completeness.}

Another question we might be able to answer is if the factor-$1.4$ penalty
claimed in~\cite{RCB16}---for complete formulas vs.\ incomplete formulas---%
% On versus as 'vs.': https://english.stackexchange.com/a/5395
% on the same Weierstraß curve
is realistic also
for optimized implementations.

In~\cite{BCLN16}, Bos, Costello, Longa, and Naehrig present performance results
for scalar multiplication on a prime-order Weierstraß curve over $\mathds{F}_{2^{256}-189}$
using parameter $a=-3$.
The curve is very similar to Curve13318 and
the implementation uses non-complete formulas for addition
and doubling.
The authors report $278\,000$ cycles for variable-base scalar multiplication on Intel Sandy Bridge.
The software in~\cite{BCLN16} is seriously optimized, and claimed to
run in constant time,
so these 
$278\,000$ cycles are reasonably comparable to our $389\,546$ cycles with
complete formulas.
In other words, this comparison affirms the factor-$1.4$ performance-penalty
claim from~\cite{RCB16}.

% -Aside from comparing the Renes-Costello-Batina formulas to Curve25519's formulas,
% -we can also look briefly at other prime-order curve implementations,
% -in order to see how the formulas fare against others.
%
% [...]
% 
% -In their paper, Renes, Costello, and Batina reported the overhead to be $1.38\times$.
% -On the other hand, \cite{BCLN16},
% -implement a similar $a=-3$ curve over $\mathds{F}_{2^{256}-189}$
% -(which they call ``w-256-mers'');
% -they chose to use incomplete formulas for their implementation,
% -as using complete formulas would incur a performance cost of a factor 2.\footnote{%
% -A relevant note to this estimate is that it was constructed \emph{before} \cite{RCB16} was published.
% -I.e.\ it is based on the formulas from Bosma and Lenstra~(\cite{BL95}),
% -\emph{not} those from Renes, Costello, and Batina~(\cite{RCB16}).
% -}
% -Their variable-basepoint scalar-multiplication runs in $278\unit{kcc}$
% -on the Sandy Bridge microarchitecture.
% -Comparing that measurement to ours, suggests that the complete formulas add---%
% -relative to their incomplete formulas based on conditional masking---%
% -an overhead of about $40\%$,
% -which strongly affirms the overhead measured by Renes, Costello, and Batina.
