#!/usr/bin/env python3

import sys
import textwrap

RULE = r'\rule{6em}{0.4pt}'

def mangle(varname):
    """
    Simple mangling of variable names, for example:
    >>> mangle('v42')
    'v_{42}'

    Leaves already-mangled stuff alone:
    >>> mangle('v_{42}')
    'v_{42}'
    """
    if varname is None:
        return None
    varname = varname.strip()
    if not varname:
        return r'v_{\texttt{undef}}'
    try:
        int(varname)
        # This value is a constant
        return varname
    except ValueError:
        pass
    if varname[0] in 'xyz':
        varname = varname[0].upper() + varname[1:]
    try:
        idx = str(int(varname[1:])).rjust(2, ' ')
        letter = varname[0]
        return f'{letter}_{{{idx}}}'
    except ValueError:
        return varname


def batch(op, *triples):
    """
    Return a latex-formatted batch of operations.

    Arguments:
        - op        Operation string specifier; can be latex.
                    Examples: '*', '+'
        - triples   Variables (c, a, b) formatting f'{c} = {a} * {b}'
                    Example: ('v3', 'v2', 'X2')
    """
    # if op == '+': out = '\\textsc{Add} &\n'
    # elif op == '-': out = '\\textsc{Sub} &\n'
    # elif op == '*': out = '\\textsc{Mul} &\n'
    # elif op == 'c': out = '\\textsc{Carry} &\n'
    # else: out = '&\n'
    out = ''

    if op.lower() == 'c':
        assert len(triples) == 1, 'carry op should only contain one list'
        valuestrs = []
        for x in triples[0]:
            if x is None:
                valuestrs.append(RULE)
                continue
            valuestr = f'${mangle(x)} \\gets \\textsc{{Red}}({mangle(x)})$'
            valuestrs.append(valuestr)

        valuesstr = ' & \n'.join(valuestrs)
        out += textwrap.indent(f'{valuesstr} \\\\\n', '  ')
        return out

    if op == '*': op = r'\cdot'

    # Construct the output string for this batch
    if op.lower() == 's':
        # Squaring looks different
        if not all(a == b for (c, a, b) in triples):
            raise AssertionError(
                'operands are not the same in squaring operation')
        triplestrs = []
        for (c, a, b) in triples:
            if any(x is None for x in (c, a, b)):
                triplestr = RULE
            else:
                triplestr = f'${mangle(c)} \\gets {mangle(a)}^2$'
            triplestrs.append(triplestr)
    else:
        triplestrs = []
        for (c, a, b) in triples:
            print(c, a, b)
            if any(x is None for x in (c, a, b)):
                triplestr = RULE
            else:
                triplestr = f'${mangle(c)} \\gets {mangle(a)} {op} {mangle(b)}$'
            triplestrs.append(triplestr)
    out += textwrap.indent('&\n'.join(triplestrs), '  ')
    out += '\\\\\n'
    return out


if __name__ != '__main__':
    print('Warning: This module is not meant to be imported', file=sys.stderr)

# ===== SANDY BRIDGE POINT ADDITION =====
sb_add = open('alg_sb_add.tex', 'w')
sb_add.write(
    batch(
        '+',
        ('v_{14}', '   X_1', '   Z_1'),
        ('v_{ 4}', '   X_1', '   Y_1'),
        ('v_{ 4}', '   X_1', '   Y_1'),
        ('v_{ 9}', '   Y_1', '   Z_1'),
    ))

sb_add.write(
    batch(
        '+',
        ('v_{15}', '   X_2', '   Z_2'),
        ('v_{ 5}', '   X_2', '   Y_2'),
        ('v_{ 5}', '   X_2', '   Y_2'),
        ('v_{10}', '   Y_2', '   Z_2'),
    ))
sb_add.write(
    batch(
        '*',
        ('v_{16}', 'v_{14}', 'v_{15}'),
        ('v_{ 1}', '   X_1', '   X_2'),
        ('v_{ 2}', '   Y_1', '   Y_2'),
        ('v_{ 3}', '   Z_1', '   Z_2'),
    ))
sb_add.write(batch(
    'c',
    ('v_{16}', 'v_{ 1}', 'v_{ 2}', 'v_{ 3}'),
))
sb_add.write(
    batch(
        '+',
        ('v_{ 7}', 'v_{ 2}', 'v_{ 1}'),
        ('v_{12}', 'v_{ 2}', 'v_{ 3}'),
        (None, None, None),
        (None, None, None),
    ))
sb_add.write(batch(
    '+',
    ('v_{17}', 'v_{ 1}', 'v_{ 3}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(batch(
    '-',
    ('v_{18}', 'v_{16}', 'v_{17}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(batch(
    '*',
    ('v_{19}', '     b', 'v_{ 3}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(batch(
    '-',
    ('v_{20}', 'v_{19}', 'v_{18}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(batch(
    '*',
    ('v_{25}', '     b', 'v_{18}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(batch(
    '*',
    ('v_{27}', '     3', 'v_{ 3}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(batch(
    '-',
    ('v_{28}', 'v_{25}', 'v_{27}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(
    batch(
        '-',
        ('v_{29}', 'v_{28}', 'v_{ 1}'),
        ('v_{v_{ 1}-v_{ 3}}', 'v_{ 1}', 'v_{ 3}'),
        (None, None, None),
        (None, None, None),
    ))
sb_add.write(
    batch(
        '*',
        ('v_{22}', '     3', 'v_{20}'),
        (None, None, None),
        ('v_{31}', '     3', 'v_{29}'),
        ('v_{34}', '     3', 'v_{v_{ 1}-v_{ 3}}'),
    ))
sb_add.write(batch(
    'c',
    (
        'v_{22}',
        None,
        'v_{31}',
        'v_{34}',
    ),
))
sb_add.write(batch(
    '-',
    ('v_{23}', 'v_{ 2}', 'v_{22}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(batch(
    '+',
    ('v_{24}', 'v_{ 2}', 'v_{22}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(
    batch(
        '*',
        ('v_{37}', 'v_{23}', 'v_{24}'),
        ('v_{36}', 'v_{31}', 'v_{34}'),
        ('v_{ 6}', 'v_{ 4}', 'v_{ 5}'),
        ('v_{11}', 'v_{ 9}', 'v_{10}'),
    ))
sb_add.write(batch(
    'c',
    (
        'v_{37}',
        'v_{36}',
        'v_{ 6}',
        'v_{11}',
    ),
))
sb_add.write(
    batch(
        '-',
        ('v_{37}', 'v_{37}', '0'),
        ('v_{36}', 'v_{36}', '0'),
        ('v_{ 8}', 'v_{ 6}', 'v_{ 7}'),
        ('v_{13}', 'v_{11}', 'v_{12}'),
    ))
sb_add.write(batch(
    '+',
    ('v_{38}', 'v_{36}', 'v_{37}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(
    batch(
        '*',
        ('v_{39}', 'v_{24}', 'v_{ 8}'),
        ('v_{42}', 'v_{33}', 'v_{ 8}'),
        ('v_{41}', 'v_{23}', 'v_{13}'),
        ('v_{35}', 'v_{31}', 'v_{13}'),
    ))
sb_add.write(batch(
    'c',
    (
        'v_{39}',
        'v_{42}',
        'v_{41}',
        'v_{35}',
    ),
))
sb_add.write(batch(
    '+',
    ('v_{43}', 'v_{41}', 'v_{42}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.write(batch(
    '-',
    ('v_{40}', 'v_{39}', 'v_{35}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_add.close()

# ===== SANDY BRIDGE POINT DOUBLING =====
sb_double = open('alg_sb_double.tex', 'w')
sb_double.write(batch(
    r'+',
    ('Y', 'Y', '0'),
    ('v_{2X}', 'X', 'X'),
            (None, None, None),
            (None, None, None),
))
sb_double.write(
    batch(
        r'*',
        ('v_{ 1}', 'X', 'X'),
        ('v_{ 6}', 'X', 'Z'),
        ('v_{ 3}', 'Z', 'Z'),
        ('v_{28}', 'Y', 'Z'),
    ))
sb_double.write(batch(
    r'c',
    ('v_{ 1}', 'v_{ 6}', 'v_{ 3}', 'v_{28}'),
))
sb_double.write(
    batch(
        r'*',
        ('v_{24}', '3', 'v_{ 1}'),
        ('v_{18}', '2b', 'v_{ 6}'),
        ("v_{ 8}'", '-\\frac{b}{2}', 'v_{ 3}'),
        ('v_{17}', '3', 'v_{ 3}'),
    ))
sb_double.write(
    batch(
        r'-',
        ('v_{25}', 'v_{24}', 'v_{17}'),
        ('v_{19}', 'v_{18}', 'v_{17}'),
        (None, None, None),
        (None, None, None),
    ))
sb_double.write(batch(
    r'-',
    ('v_{20}', 'v_{ 1}', 'v_{19}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_double.write(batch(
    r'*',
    ('v_{22}', '-3', 'v_{20}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_double.write(batch(
    r'+',
    ('v_{ 9}', "v_{ 8}'", 'v_{ 6}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_double.write(
    batch(
        r'*',
        ('v_{11}', '-6', 'v_{ 9}'),
        ('v_{34}', '8', 'v_{28}'),
        (None, None, None),
        (None, None, None),
    ))
sb_double.write(batch(
    r'c',
    ('v_{11}', 'v_{34}', 'v_{22}', 'v_{25}',),
))
sb_double.write(batch(
    r'+',
    ('v_{29}', 'v_{28}', 'v_{28}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_double.write(
    batch(
        r'*',
        ('v_{30}', 'v_{22}', 'v_{29}'),
        ('v_{26}', 'v_{22}', 'v_{25}'),
        ('v_{ 2}', 'Y', 'Y'),
        ('v_{ 5}', 'v_{2X}', 'Y'),
    ))
sb_double.write(batch(
    r'c',
    ('v_{30}', 'v_{26}', 'v_{ 2}', 'v_{ 5}'),
))
sb_double.write(batch(
    r'-',
    ('v_{12}', 'v_{ 2}', 'v_{11}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_double.write(batch(
    r'+',
    ('v_{13}', 'v_{ 2}', 'v_{11}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_double.write(
    batch(
        r'*',
        ('v_{32}', 'v_{ 2}', 'v_{34}'),
        ('v_{15}', 'v_{ 5}', 'v_{12}'),
        ('v_{14}', 'v_{12}', 'v_{13}'),
        (None, None, None),
    ))
sb_double.write(batch(
    r'c',
    ('v_{32}', 'v_{15}', 'v_{14}', None),
))
sb_double.write(batch(
    r'-',
    ('v_{31}', 'v_{15}', 'v_{30}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_double.write(batch(
    r'+',
    ('v_{27}', 'v_{14}', 'v_{26}'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
sb_double.close()

# ===== HASWELL POINT ADDITION =====
h_add = open('alg_h_add.tex', 'w')
h_add.write('% We have to immediately carry Y, because it may just have been\n'
            '% negated, in which case it would be too large to multiply.\n')
h_add.write(batch(
    'c',
    ('Y_1', 'Y_2', 'Y_1', 'Y_2'),
))
h_add.write(
    batch(
        '+',
        ('v14', 'x1', 'z1'),
        ('v4', 'x1', 'y1'),
        ('v4', 'x1', 'y1'),
        ('v9', 'y1', 'z1'),
    ))
h_add.write(
    batch(
        '+',
        ('v15', 'x2', 'z2'),
        ('v5', 'x2', 'y2'),
        ('v5', 'x2', 'y2'),
        ('v10', 'y2', 'z2'),
    ))
h_add.write(
    batch(
        '*',
        ('v16', 'v14', 'v15'),
        ('v1', 'x1', 'y2'),
        ('v2', 'y1', 'z2'),
        ('v3', 'z1', 'z2'),
    ))
h_add.write(batch(
    'c',
    ('v16', 'v1', 'v2', 'v3'),
))
h_add.write(batch(
    '+',
    ('v17', 'v1', 'v3'),
))
h_add.write(batch(
    '+',
    ('v7', 'v1', 'v2'),
))
h_add.write(batch(
    '+',
    ('v12', 'v2', 'v3'),
))
h_add.write(batch(
    '-',
    ('v18', 'v16', 'v17'),
))
h_add.write(batch(
    '*',
    ('v19', 'b', 'v3'),
))
h_add.write(batch(
    '*',
    ('v25', 'b', 'v18'),
))
h_add.write(batch(
    '-',
    ('v20', 'v18', 'v19'),
))
h_add.write(batch(
    '*',
    ('v22', '3', 'v20'),
))
h_add.write(batch(
    '+',
    ('v24', 'v22', 'v2'),
))
h_add.write(batch(
    '-',
    ('v23', 'v2', 'v22'),
))
h_add.write(batch(
    '*',
    ('v27', '3', 'v3'),
))
h_add.write(batch(
    '-',
    ('v_{v_{25} - v_{1}}', 'v25', 'v1'),
))
h_add.write(batch(
    '-',
    ('v29', 'v_{v_{25} - v_{1}}', 'v27'),
))
h_add.write(batch(
    '*',
    ('v31', '3', 'v29'),
))
h_add.write(batch(
    '*',
    ('v33', '3', 'v1'),
))
h_add.write(batch(
    '-',
    ('v34', 'v33', 'v27'),
))
h_add.write(batch(
    '+',
    ('v34', 'v34', '2^{32}p'),
    ('v24', 'v24', '2^{32}p'),
    ('v31', 'v31', '2^{32}p'),
    ('v23', 'v23', '2^{32}p'),
))
h_add.write('% \\Comment{Force values into positive domain.}\n')
h_add.write(batch(
    'c',
    ('v34', 'v24', 'v31', 'v23'),
))
h_add.write(
    batch(
        '*',
        ('v36', 'v34', 'v31'),
        ('v37', 'v24', 'v23'),
        ('v6', 'v4', 'v5'),
        ('v11', 'v9', 'v10'),
    ))
h_add.write(batch(
    'c',
    ('v36', 'v37', 'v6', 'v11'),
))
h_add.write(batch(
    '+',
    ('v38', 'v36', 'v37'),
))
h_add.write(batch(
    '-',
    (None, None, None),
    (None, None, None),
    ('v7', 'v7', '4p'),
    ('v12', 'v12', '4p'),
))
h_add.write(batch(
    '-',
    (None, None, None),
    (None, None, None),
    ('v8', 'v6', 'v7'),
    ('v13', 'v11', 'v12'),
))
h_add.write(
    batch(
        '*',
        ('v42', 'v8', 'v34'),
        ('v39', 'v8', 'v24'),
        ('v35', 'v13', 'v31'),
        ('v41', 'v13', 'v23'),
    ))
h_add.write(batch(
    '+',
    (None, None, None),
    ('v39', 'v39', '2^{37}p'),
    (None, None, None),
    (None, None, None),
))
h_add.write(batch(
    '-',
    (None, None, None),
    ('v40', 'v39', 'v35'),
    (None, None, None),
    (None, None, None),
))
h_add.write(batch(
    '+',
    ('v43', 'v42', 'v41'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
h_add.write(batch(
    'c',
    ('v43', 'v40', None, None),
))
h_add.close()

# ===== HASWELL POINT DOUBLING =====
h_double = open('alg_h_double.tex', 'w')
h_double.write(batch(
    '+',
    ('v_{X+Z}', 'X', 'Z'),
    ('v_{X+Z}', 'X', 'Z'),
    ('v_{X+Z}', 'X', 'Z'),
    ('v_{X+Z}', 'X', 'Z'),
))
h_double.write(batch(
    '+',
    ('v_{2Y}', 'Y', 'Y'),
    ('v_{2Y}', 'Y', 'Y'),
    ('v_{2Y}', 'Y', 'Y'),
    ('v_{2Y}', 'Y', 'Y'),
))
h_double.write(
    batch(
        's',
        ('v_{(X+Z)^2}', 'v_{X+Z}', 'v_{X+Z}'),
        ('v1', 'X', 'X'),
        ('v2', 'Y', 'Y'),
        ('v3', 'Z', 'Z'),
    ))
h_double.write(batch(
    'c',
    ('v_{(X+Z)^2}', 'v1', 'v2', 'v3'),
))
h_double.write(batch(
    '-',
    ('v_{Z^2 + 2XZ}', 'v_{(X+Z)^2}', 'v1'),
))
h_double.write(batch(
    '-',
    ('v7', 'v_{Z^2 + 2XZ}', 'v3'),
))
h_double.write(batch(
    '*',
    ('v18', 'b', 'v7'),
))
h_double.write(batch(
    '*',
    ('v8', 'b', 'v3'),
))
h_double.write(batch(
    '*',
    ('v17', '3', 'v3'),
))
h_double.write(batch(
    '-',
    ('v19', 'v18', 'v17'),
))
h_double.write(batch(
    '-',
    ('v9', 'v8', 'v7'),
))
h_double.write(batch(
    '*',
    ('v24', '3', 'v1'),
))
h_double.write(batch(
    '*',
    ('v11', '3', 'v9'),
))
h_double.write(batch(
    '-',
    ('v20', 'v19', 'v1'),
))
h_double.write(batch(
    '*',
    ('v22', '3', 'v20'),
))
h_double.write(batch(
    '-',
    ('v12', 'v2', 'v11'),
))
h_double.write(batch(
    '+',
    ('v13', 'v2', 'v11'),
))
h_double.write(batch(
    '-',
    ('v25', 'v24', 'v17'),
))
h_double.write(batch(
    '*',
    ('v_{4v_2}', '4', 'v_2'),
))
h_double.write(batch(
    '+',
    ('v22', 'v22', '2^{32}p'),
    ('v12', 'v12', '2^{32}p'),
    ('v25', 'v25', '2^{32}p'),
    ('v13', 'v13', '2^{32}p'),
))
h_double.write('% \\Comment{Force values into positive domain.}\n')
h_double.write(batch(
    'c',
    ('v22', 'v12', 'v25', 'v13'),
))
h_double.write(
    batch(
        '*',
        ('v26', 'v22', 'v25'),
        ('v14', 'v12', 'v13'),
        ('v28', 'v_{2Y}', 'Z'),
        ('v4', 'v_{2Y}', 'X'),
    ))
h_double.write(batch(
    'c',
    ('v26', 'v14', 'v28', 'v4'),
))
h_double.write(batch(
    '+',
    ('v27', 'v26', 'v14'),
    (None, None, None),
    (None, None, None),
    (None, None, None),

))
h_double.write(
    batch(
        '*',
        ('v30', 'v28', 'v22'),
        ('v15', 'v4', 'v12'),
        ('v34', 'v_{4v_2}', 'v28'),
        (None, None, None),
    ))
h_double.write(batch(
    '-',
    ('v30', 'v30', '2^{37}p'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
h_double.write(batch(
    '-',
    ('v31', 'v15', 'v30'),
    (None, None, None),
    (None, None, None),
    (None, None, None),
))
h_double.write(batch(
    'c',
    ('v31', 'v34', None, None),
))
h_double.close()
