\section{Introduction}
\label{sec:intro}

Since its invention in 1985, independently by Koblitz~\cite{Kob87} and Miller~\cite{Mil86},
elliptic-curve cryptography (ECC) has widely been accepted as the state of the art
in asymmetric cryptography. 
This success story is mainly due to the fact that attacks 
have not substantially improved: with a choice of elliptic curve that was in the 80s
already considered conservative, the best attack for solving the elliptic-curve
discrete-logarithm problem is still the generic Pollard-rho algorithm 
(with minor speedups, for example by exploiting the efficiently computable
negation map in elliptic-curve groups~\cite{BLS11}).

One of the main developments since the first generation of elliptic-curve cryptography
has been in the choice of curves.
Until 2006, the widely accepted choice of elliptic curve was a prime-order Weierstraß curve.
In 2006, Bernstein proposed the Montgomery curve ``Curve25519''%
\footnote{In the 2006 paper, Curve25519 referred to the ECDH key-exchange protocol;
Bernstein later recommended to use X25519 for the protocol and Curve25519 for the underlying curve~\cite{Ber14}.} 
as an alternative offering multiple security and performance features.
Most importantly---at least in the context of this paper---%
it featured highly efficient \emph{complete formulas}, 
which enable fast arithmetic without any checks for special cases.
These complete formulas are somewhat limited, because they only cover differential addition.
This is not a problem for the X25519 elliptic-curve
Diffie-Hellman key exchange presented in~\cite{Ber06}, but it makes implementation
of more advanced protocols (like signature schemes) somewhat inconvenient.

This issue was addressed through a sequence of three papers.
In~\cite{Edw07}, Edwards introduced a new description of elliptic curves;
in~\cite{BL07}, Bernstein and Lange presented very efficient complete formulas
for group arithmetic on these curves (and introduced the name ``Edwards curves'');
and in~\cite{BBJ+08}, Bernstein, Birkner, Joye, Lange, and Peters 
generalized the concept to twisted Edwards curves and showed
that this class of elliptic curves is birationally equivalent to Montgomery curves.
The twisted Edwards form of Curve\-25519 was subsequently used in~\cite{BDL+11,BDL+12} 
for the construction of the Ed25519 digital-signature scheme.

The simplicity and efficiency of X25519 key exchange and Ed25519 signatures
resulted in quick adoption in a variety of applications, such as 
SSH, the Signal protocol, and the Tor anonymity project.
Both schemes are also used in TLS~1.3.

\subheading{Complete addition or prime order.}
Unfortunately, 
the advantages of Montgomery and twisted Edwards curves---%
most notably the very efficient complete addition formulas---%
have to be weighed against a disadvantage:
the group of points cannot have prime order 
as it always has a cofactor of a multiple of 4.
Consequently, a somewhat simplified view on choosing curves for cryptographic applications
is that we have to choose between either efficient complete formulas
through Montgomery or (twisted) Edwards curves,
or prime-order groups through Weierstraß curves.

The design of X25519 and Ed25519 carefully takes the non-trivial cofactor into account.
However in more involved protocols,
a non-trivial cofactor may complicate the protocol's design,
potentially leading to security issues.
In the last years, we saw at least two examples of protocols
deployed in real-world applications that had catastrophic vulnerabilities 
because they did not carefully handle the cofactor.

The first example was a vulnerability in the Monero cryptocurrency,
that allowed for arbitrary double spending~\cite{LS17}.
Monero requires that ``key images''%
---which bind transactions to their sender's public key---%
should be non-malleable,  i.e.\ for a transaction to be valid,
its public key must be unique.
Unfortunately, due to the cofactor,
an attacker could construct different key images
that were bound to the same public key,
therefore allowing arbitrary double-spending.
This issue was mitigated by checking the order of the key image,
which involves a full scalar multiplication, ironically diminishing the
performance that Curve\-25519 was meant to provide.

The second example was recently discovered by Cremers and Jackson, 
who found more vulnerabilities in protocols caused by the non-trivial cofactor~\cite{CJ19}. 
These vulnerabilities allowed attackers to bypass the authentication
properties of the Secure Scuttlebutt Gossip protocol and Tendermint's secure handshake.

\subheading{Why not both?}
In 2015, Hamburg presented the ``Decaf'' technique~\cite{Ham17}, 
which removes the cofactor of twisted Edwards curves through a clever encoding.
He later refined the technique to ``Ristretto'' (see ~\cite{ristretto}), which is
now proposed in the crypto forum research group (CFRG) of IETF for standardization~\cite{VGT+19}.
The Decaf and Ristretto encodings come at some computational cost and also added
complexity of the implementation, but it eliminates the burden 
of handling the cofactor in protocol design.

However, there is another, much more obvious, approach to complete
addition in prime-order elliptic-curve groups.
It is long known that complete addition formulas also exist
for Weierstraß curves~\cite{BL95}, but
those formulas were long regarded as too inefficient to offer an acceptable tradeoff.
The situation changed in 2016, when Renes, Costello, and Batina revisited the approach
from~\cite{BL95} and presented much more efficient complete addition
formulas for Weierstraß curves~\cite{RCB16}. 
Unfortunately, these formulas are still considerably less efficient
than the incomplete addition formulas that possibly require handling of 
special cases.
The performance gap is even larger compared to
the complete addition
formulas for twisted Edwards curves, and the complete differential
additional used in the scalar-multiplication ladder on Montgomery
curves.

\subheading{Contributions of this paper.}
  If we assume the usage of complete addition formulas for both---%
  twisted Edwards or Montgomery curves on one hand
  and Weierstraß curves on the other hand---%
  the choice of curve becomes a tradeoff between performance and protocol simplicity.
  To fully understand this tradeoff, we need to know how large exactly
  the performance penalty is for using Weierstraß curves with the
  complete addition formulas from~\cite{RCB16}.
  The standard approach to understand performance differences is
  to compare the speed of optimized implementations,
  ideally on different target architectures.
  Almost surprisingly however, there are no such optimized implementations of
  elliptic-curve scalar multiplication using complete formulas on Weierstraß curves.
  In this paper we present such implementations and answer the question about the
  actual cost of complete cofactor-1 ECC arithmetic using the formulas
  from~\cite{RCB16}.

  More specifically, we present highly optimized software targeting three
  different microarchitectures for variable-basepoint scalar multiplication
  on a 255-bit Weierstraß
  curve over the field $\mathds{F}_{2^{255}-19}$, the same field
  underlying Curve\-25519.
  Choosing a curve over the same field eliminates possible effects that are
  not due to the choice of curve shape and corresponding addition formulas,
  but due to differences in speed of the field arithmetic.

  The three microarchitectures we are targeting are
  Intel's 64-bit Haswell generation of processors featuring AVX2 vector instructions; 
  the earlier Intel Ivy Bridge processors that feature AVX vector instructions,
  but not yet the AVX2 integer-vector instruction set; and
  the ARM Cortex M4 family of 32-bit embedded microcontrollers.
  All our implementations follow the ``constant-time'' approach of
  avoiding secret branch conditions and secretly indexed memory access.
  
  We compare our results to scalar multiplication from highly optimized
  X25519 software on the same microarchitectures.
  Perhaps surprisingly the performance penalty heavily depends
  on the microarchitecture; it ranges between a factor of only 1.47 on
  Intel Haswell and a factor of 2.87 on ARM Cortex M4.

  \subheading{Disclaimer.}
  This paper revisits the discussion of the performance of Curve\-25519 (and Curve\-448)
  relative to the old Weierstraß curves.
  We see a certain risk that the results in this paper may be misinterpreted one way or another,
  so we would like to clarify our intentions, and how we see the results of this paper:
  \begin{itemize}
    \item The motivation for this work has been that we saw a potentially interesting
      missing data point in the context of choosing elliptic curves for
      cryptographic applications.
    \item We do not think that any elliptic-curve standardization
      discussion should be re-opened. No result in this
      paper suggests that this would be useful and we believe that the choice of Curve\-25519 and
      Curve448 by IETF was a very sensible one. All effort that the community
      can invest in standardization is better placed in, for example, efforts to
      choose sensible post-quantum primitives.
    \item
      We see a rather common misconception of Weierstraß curves not having any
      (practical) complete addition formulas.
      For example, the book ``Serious Cryptography'' by Aumasson 
      describes the ANSSI and Brainpool curves (both prime-order Weierstraß curves) as
      \emph{``two families of curves that don't support complete addition formulas [...]''}~\cite[page 231]{Aum17SeriousCryptography}.
      In similar spirit, Bernstein and Lange on their ``SafeCurves'' website~\cite{safecurves}, 
      dismiss Weierstraß curves entirely as a viable option for cryptographic applications based on the ground that 
      complete addition is so much less efficient than incomplete addition formulas
      (and even less efficient than complete addition on twisted Edwards curves). 
      In our opinion, a sensibly chosen Weierstraß curve using the complete addition formulas
      from~\cite{RCB16} or~\cite{SM17} may well be the \emph{safer} choice for protocols and
      applications that can live with the performance penalty.
    \item While we think that it is always preferable to use complete addition
    formulas for implementing Weierstraß-curve arithmetic,
    we would like to articulate that by no means
    we recommend the use of the Renes-Costello-Batina formulas above
    the use of Curve25519 with Ristretto for \emph{new} protocols.
    Indeed, we support the proposal brought into CRFG 
    by de Valence, Grigg, Tankersley, Valsorda, and Lovecruft~\cite{VGT+19}
    for the standardization of the Ristretto encoding.
  \end{itemize}

  \subheading{Related work.}
  The most relevant related work for this paper can be grouped in two
  categories: papers presenting optimized implementations of Curve25519
  and papers investigating performance of complete group addition on Weierstraß curves.

  In the first category, the directly related papers present results for
  optimized scalar multiplication on Curve25519 targeting the same microarchitectures
  that we target in this paper. 
  To the best of our knowledge, the current speed record for X25510 
  on Intel Sandy Bridge and Ivy Bridge processors is held by the ``Sandy2x''
  software by Chou~\cite{Cho16}.
  The speed record for the Intel Haswell microarchitecture is held by the software by 
  Oliveira, López, H{\i}\c{s}{\i}l, Faz-Hern\'andez, Rodr\'iguez-Henr\'iquez presented in~\cite{OLH+18}.
  This paper also presents even higher speeds for the Intel Skylake microarchitecture;
  that software makes use of the \texttt{MULX} and \texttt{ADCX/ADOX} instructions that
  are not available on Haswell.
  Finally, the speed record for scalar multiplication on Curve25519 on Cortex-M4 is
  held by software presented by Haase and Labrique in~\cite{HL19}.
  We provide a comparison of our results with the results from those papers
  in Section~\ref{sec:results}.
  
  In the second category we are aware of only three results: 
  In~\cite{RCB16}, Renes Costello, and Batina provide benchmarks of scalar multiplication
  on various NIST-P curves~\cite{FIPS186-4} in OpenSSL~\cite{openssl} 
  using their complete formulas and compare them with the
  ``standard'' incomplete formulas used by default in OpenSSL. 
  This comparison shows a performance penalty of a factor of about $1.4$.
  However, the figures in~\cite[Table~2]{RCB16} strongly suggest that the
  comparison did not use the optimized implementation of scalar
  multiplication on the NIST curves that would need to be enabled with
  the configure option \verb|enable-ec_nistp_64_gcc_128| when building OpenSSL.
  In~\cite{CRB16}, Costa Massolino, Renes, and Batina present an FPGA implementation
  of scalar multiplication on arbitrary Weierstraß curves over prime-order fields using
  the complete formulas from~\cite{RCB16}. They claim that their results are
  ``competitive with current literature'', but also state that 
  ``it is not straightforward to do a well-founded comparison among works in the literature''.
  This is because hardware implementations have a much larger design space,
  not only with tradeoffs between area and speed, but also flexibility with regards
  to the supported elliptic curves (e.g., through different curve shapes or 
  support for specialized or generic field arithmetic).
  Finally, in~\cite{SM17} Susella and Montrasio estimate performance of different
  scalar-multiplication approaches. They report estimates in terms of multiplications
  per scalar bit, assuming that 
  multiplication costs as much as squaring and multiplication by (small) constants, 
  and that addition costs 10\% of a multiplication.
  In this metric the ladder from \cite{SM17} is very slightly cheaper at $19.1$ than
  scalar multiplication using the formulas from~\cite{RCB16} at $19.33$. 
  However, if we understand correctly, the estimates for~\cite{RCB16} are computed
  without taking into account \emph{signed} fixed-window scalar multiplication.
  In this metric, the Montgomery ladder used in X25519 software would come at a cost
  of~$10.8$.

  \subheading{Notation.}
  We use abbreviations $\mathbf{M}$ to refer to the cost of a finite-field multiplication,
  use $\mathbf{S}$ to refer to the cost of a squaring, $\mathbf{a}$ to refer to the cost
  of an addition, and $\mathbf{m_c}$ to refer to the cost of multiplication by a constant $c$.
  
  \subheading{Availability of software.}
  We place all software related to this paper into the public domain
  (to the maximum extent possible, using the Creative Commons CC0 waiver).
  The code packages are published through the public repository at
  \url{https://github.com/dsprenkels/curve13318-all}.

  \subheading{Organization of this paper.}
  In Section~\ref{sec:prelim} we give a brief review of the mathematical background
  on elliptic curves and in particular motivate our choice of curve, which we call Curve13318.
  In Section~\ref{sec:implementation} we provide details of our implementations
  of constant-time variable-basepoint scalar multiplication on Curve13318 for
  Intel Sandy Bridge, Intel Haswell, and ARM Cortex M4.
  Section~\ref{sec:results} presents the performance results and compares to
  state-of-the-art implementations of scalar multiplication on Curve25519.
  We conclude the paper and give an overview of possible future work in Section~\ref{sec:conclusion}.

